//bài1
const arrColors = ["green", "black", "red", "pink", "brown", "gray", "blue",]

let outPut = ''
for (let i = 0; i < arrColors.length; i++) {
    let color = arrColors[i]
    outPut += ` <button
                    class = "btn ml-2 mt-3 text-white"
                    style = "background-color:${color}"
                    onclick = changeColors("${color}")>
                ${color}
                </button>`
}
document.getElementById("colors").innerHTML = outPut

window.changeColors = (color) => {
    document.getElementById("home").style.color = color
}



//bài 2
const tinhTrungBinh = (mangSo) => {
    const sum = mangSo.reduce((partialSum, a) => partialSum + a, 0)
    const avg = sum / mangSo.length
    const num = avg.toFixed(2);

    return num
}

const trungBinhCong1 = () => {
    let toanValue = document.getElementById("txt-toan").value * 1
    let lyValue = document.getElementById("txt-ly").value * 1
    let hoaValue = document.getElementById("txt-hoa").value * 1

    const result = tinhTrungBinh([toanValue, lyValue, hoaValue])

    document.getElementById("result1").innerHTML = result
}


const trungBinhCong2 = () => {
    let vanValue = document.getElementById("txt-van").value * 1
    let suValue = document.getElementById("txt-su").value * 1
    let diaValue = document.getElementById("txt-dia").value * 1
    let anhValue = document.getElementById("txt-anh").value * 1

    const result = tinhTrungBinh([vanValue, suValue, diaValue, anhValue])

    document.getElementById("result2").innerHTML = result

}


//bài3
const hoverText = () => {
    let heading = "HoverMe!"
    let tach = [...heading]
    console.log(tach);
    let item = ""
    for (let i = 0; i < tach.length; i++) {
        item += `<span class="text">${tach[i]}</span>`
    }
    document.getElementById("exOutPut").innerHTML = item
}


hoverText()



